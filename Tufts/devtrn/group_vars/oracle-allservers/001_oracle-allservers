---
sys_app_user:               "oracle"
sys_app_user_uid:           54322
sys_app_group:              "dba"
sys_app_group_gid:          54322

target_swapspace_gb:        "{{ override_oracle_target_swapspace_gb | default (16) }}"
swap_on_separate_disk:      true
swap_new_disk:              "sdf"
swap_new_partition_number:  "1"

firewalld_openports_group:
  - "1521/tcp"



oracle_sid: "{{ customer_acronym|upper }}C{{ oracle_prefix_code|upper }}{{ env_type_acronym|upper }}"
oracle_pdb_name: "{{ customer_acronym|upper }}P{{ oracle_prefix_code|upper }}{{ env_type_acronym|upper }}"

oracle_base_dir: /u01/app/oracle
oracle_base: "{{ oracle_base_dir }}/product"
oracle_home_dir: "/home/{{ sys_app_user }}"
oracle_db_state: present
oracle_stage: "{{ oracle_home_dir }}/binaries"
oracle_stage_remote: "{{ oracle_stage }}"
oracle_scripts_dir: "{{ oracle_home_dir }}/oracle_scripts"
redo_log_size_in_mb: 500
oracle_db_mem_totalmb: 1024
open_cursors: 12000
oracle_version: 12.1.0.2
oracle_version_base: "{{ oracle_version | regex_replace('^(\\d+\\.\\d+\\.\\d+)\\.\\S+', '\\1') }}"
oracle_he_dump_dir: "{{ oracle_home_dir }}/HE_DUMP"

oracle_dbf_dir_fs:  /u01/app/oracle/oradata/                   # If storage_type=FS this is where the database is placed.
oracle_reco_dir_fs: /u01/app/oracle/fast_recovery_area         # If storage_type=FS this is where the fast recovery area is placed.

# For VRA, EPEL repo cannot be installed from RPM. he-yum-internal-repo installs yum proxy for EPEL
configure_epel_repo: "{{ false if provisioner == 'vra' else true }}"

oracle_home: db_1
# RELENG-1248: HealthEdge DBA standard forces us to use ORACLE_HOME in format $ORACLE_BASE/product/12.1.0/db_1
oracle_home_db: "{{ oracle_base }}/{{ oracle_version_base }}/{{ oracle_home }}"

# TODO: Figure out passwords
ops_utils_hetools:
ops_utils_cronjobs: {}
sw_size: 120G
container_size: 120G

data_size: 180G
index_size: 60G
configure_host_disks: yes
host_fs_layout:
  - vgname: vgOracleSW
    state: present
    filesystem:
      - {mntp: '{{ oracle_base_dir }}', lvname: lvSW, lvsize: 100%FREE, fstype: xfs}
    disk:
      - device: "{{ '/dev/xvdf' if provisioner == 'aws' else '/dev/sdb' }}"
        pvname: "{{ '/dev/xvdf1' if provisioner == 'aws' else '/dev/sdb1' }}"
  - vgname: vgOracleContainer
    state: present
    filesystem:
      - {mntp: '/u01/oradata/{{ oracle_sid }}', lvname: lvContainer, lvsize: 100%FREE, fstype: xfs}
    disk:
      - device: "{{ '/dev/xvdg' if provisioner == 'aws' else '/dev/sdd' }}"
        pvname: "{{ '/dev/xvdg1' if provisioner == 'aws' else '/dev/sdd1' }}"
  - vgname: 'vgOracleData{{ env_type_acronym|upper }}'
    state: present
    filesystem:
      - {mntp: '/u02/oradata/{{ oracle_pdb_name }}', lvname: lvData, lvsize: 100%FREE, fstype: xfs}
    disk:
      - device: "{{ '/dev/xvdh' if provisioner == 'aws' else '/dev/sde' }}"
        pvname: "{{ '/dev/xvdh1' if provisioner == 'aws' else '/dev/sde1' }}"
  - vgname: 'vgOracleIndex{{ env_type_acronym|upper }}'
    state: present
    filesystem:
      - {mntp: '/u03/oradata/{{ oracle_pdb_name }}', lvname: lvIndex, lvsize: 100%FREE, fstype: xfs}
    disk:
      - device: "{{ '/dev/xvdi' if provisioner == 'aws' else '/dev/sdc' }}"
        pvname: "{{ '/dev/xvdi1' if provisioner == 'aws' else '/dev/sdc1' }}"

oracle_databases:
  - home: "{{ oracle_home }}"
    oracle_version_db: "{{ oracle_version }}"
    oracle_edition: EE
    oracle_db_name: "{{ oracle_sid }}"
    oracle_db_passwd: "{{ oracle_container_password }}"
    oracle_db_type: SI
    is_container: True
    pdb_prefix: pdb
    num_pdbs: 0
    storage_type: FS
    oracle_db_mem_percent: 70
    automatic_memory_management: TRUE
    service_name: orcl_serv
    oracle_init_params: "open_cursors={{ open_cursors }}"
    oracle_database_type: MULTIPURPOSE
    redolog_size_in_mb: "{{ redo_log_size_in_mb }}"
    listener_name: LISTENER
    state: "{{ oracle_db_state }}"
    init_params: "db_block_size=8192,processes=4096,cursor_sharing=FORCE,db_cache_advice=OFF,db_file_multiblock_read_count=128,query_rewrite_enabled=TRUE,query_rewrite_integrity=TRUSTED,optimizer_index_cost_adj=1"
  - home: "{{ oracle_home }}"
    dbca_operation: createPluggableDatabase
    oracle_version_db: "{{ oracle_version }}"
    oracle_edition: EE
    oracle_db_name: "{{ oracle_pdb_name }}"
    oracle_db_type: SI
    oracle_db_passwd: "{{ oracle_pluggable_password }}"
    container_db_name: "{{ oracle_sid }}"
    datafile_dest: "/u02/oradata/{{ oracle_pdb_name }}"
    oracle_database_type: MULTIPURPOSE
    is_container: False
    num_pdbs: 1
    storage_type: FS
    oracle_db_mem_percent: 70
    automatic_memory_management: TRUE
    service_name: orcl_serv
    oracle_init_params: "open_cursors={{ open_cursors }}"
    init_params: "db_block_size=8192,processes=4096,cursor_sharing=FORCE,db_cache_advice=OFF,db_file_multiblock_read_count=128,query_rewrite_enabled=TRUE,query_rewrite_integrity=TRUSTED,optimizer_index_cost_adj=1"
    redolog_size_in_mb: "{{ redo_log_size_in_mb }}"
    listener_name: LISTENER
    state: "{{ oracle_db_state }}"

# 'oracle_group' is used by third-party oracle roles.... over-riding them here to change those
# as little as possible
oracle_user:    "{{ sys_app_user  }}"
oracle_group:   "{{ sys_app_group }}"

